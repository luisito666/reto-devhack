import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';


import { InfiniteScrollModule } from 'ngx-infinite-scroll';

// importando el router
import { APP_ROUTING } from './router.app';

// modulo de servicios
import { ServicesModule } from './services/service.module';

//import { MainService } from './services/main.services';

// App component principal
import { AppComponent } from './app.component';

// modulo de las paginas
import { PagesModule } from './components/pages/page.module';

// modulo de los componentes compartidos
import { SharedModule } from './components/shared/shared.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    JsonpModule,
    InfiniteScrollModule,
    APP_ROUTING,
    PagesModule,
    SharedModule,
    ServicesModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
