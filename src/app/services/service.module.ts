import { NgModule } from '@angular/core';

import { MainService } from './main.services';


@NgModule({
    imports:[],
    exports:[],
    declarations:[],
    providers:[]
})

export class ServicesModule {
    static forRoot() {
        return {
            ngModule: ServicesModule,
            providers:[
                MainService
            ] 
        }
    }
}

export {
    MainService
}
