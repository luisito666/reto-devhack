import { Injectable } from '@angular/core';
import { Http, Jsonp, Headers } from '@angular/http';

import { map } from 'rxjs/operators'; // Map

@Injectable()
export class MainService {

    private api_url = "https://rickandmortyapi.com/api";
    public characters: any ;
    public random_characters: any ;
    public detail: any ;

    constructor(private _http: Http){}

    get_character_random_page() {
        
        let random_number = Math.floor(Math.random()*25) + 1;

        let url = `${this.api_url}/character/?page=${random_number}`;

        return this._http.get(url)
            .pipe(map( res => {
                this.random_characters = res.json().results;
            }));

    }

    get_character_page(page:any) {

        let url = `${this.api_url}/character/?page=${page}`;

        return this._http.get(url)
            .pipe(map( res => {
                this.characters = res.json().results;
            }));

    }

    get_character_page_scroll(page) {
        let url = `${this.api_url}/character/?page=${page}`;

        return this._http.get(url)
            .pipe(map( res => {
                res.json().results.forEach(element => {
                    this.characters.push(element);
                });
            }));
    }

    get_character_detail(id) {

        let url = `${this.api_url}/character/${id}`;

        return this._http.get(url)
            .pipe(map(res => {
                this.detail = res.json();
            }));

    }

}