import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/pages/home/home.component';
import { DetailComponent } from './components/pages/detail/detail.component';
import { CharactersComponent } from './components/pages/characters/characters.component';


const APP_ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'detail/:id', component: DetailComponent },
    { path: 'characters', component: CharactersComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
]


export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);