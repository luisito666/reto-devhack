import { Component, OnInit } from '@angular/core';
import { MainService } from '../../../services/main.services';


@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styles: []
})
export class CharactersComponent implements OnInit {

  constructor(public svc: MainService) {
    if (!svc.characters){
      this.svc.get_character_page(1).subscribe(data => {
        console.log(svc.characters)
      })
    }
    
  }

  ngOnInit() {
  }

}
