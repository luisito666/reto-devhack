import { Component, OnInit } from '@angular/core';
import { MainService } from '../../../services/main.services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {

  constructor(
    public _http: MainService,
    private router: ActivatedRoute
    ) {
      this.router.params.subscribe(params => {
        this._http.get_character_detail(params['id']).subscribe();
      });
    
  }

  ngOnInit() {
  }

}
