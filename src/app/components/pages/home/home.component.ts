import { Component, OnInit } from '@angular/core';
import { MainService } from '../../../services/main.services';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  loading: boolean = true;
  public characters:any[] = [];

  constructor(private svc: MainService) {
    this.svc.get_character_random_page().subscribe( () => {
      // this.getThreeRamdom();
      let random_number = Math.floor(Math.random()*21) + 4;
      this.characters = this.svc.random_characters.slice(random_number - 3,random_number);
      this.loading = false;
    });
  }

  getThreeRamdom(){
    
    //console.log(this.characters);
  }

  ngOnInit() {
  }

}
