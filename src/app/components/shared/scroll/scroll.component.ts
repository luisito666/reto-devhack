import { Component, OnInit } from '@angular/core';
import { MainService } from '../../../services/main.services';

@Component({
  selector: 'app-scroll',
  templateUrl: './scroll.component.html',
  styles: []
})
export class ScrollComponent implements OnInit {
  
  control:number = 1;

  constructor(private svc: MainService) { }

  ngOnInit() {
  }

  onScroll() {
    console.log('scroll')
    this.control = this.control + 1;
    this.svc.get_character_page_scroll(this.control).subscribe();
  }

}
