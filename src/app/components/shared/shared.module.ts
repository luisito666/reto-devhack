import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CardsComponent } from './cards/cards.component';
import { NavComponent } from './nav/nav.component';
import { ScrollComponent } from './scroll/scroll.component';
import { LoaderComponent } from './loader/loader.component';


import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        InfiniteScrollModule,
    ],
    declarations: [
        CardsComponent,
        NavComponent,
        ScrollComponent,
        LoaderComponent
    ],
    exports: [
        CardsComponent,
        NavComponent,
        ScrollComponent,
        LoaderComponent
    ]
})

export class SharedModule {}
